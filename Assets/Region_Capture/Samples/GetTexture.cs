﻿using UnityEngine;
using System.Collections;

public class GetTexture : MonoBehaviour {
	private Camera RenderTextureCamera;
	public static bool verde;
	private Texture2D textura;
	public Material material;
	public bool estado = false;
	private GameObject obj;

	void Start(){
		obj = GameObject.Find("Render_Texture_Camera");
	}
	public IEnumerator test(){
		material.mainTexture = null;
		yield return new WaitForSeconds (0.6f);
		RenderTexture.active = obj.GetComponent<Camera>().targetTexture;
		Texture2D tex = new Texture2D (obj.GetComponent<Camera>().targetTexture.width,obj.GetComponent<Camera>().targetTexture.height);
		tex.ReadPixels(new Rect(0, 0, obj.GetComponent<Camera>().targetTexture.width, obj.GetComponent<Camera>().targetTexture.height), 0, 0);
		tex.Apply();
		material.mainTexture = tex;
		obj.transform.parent.GetComponent<MeshRenderer>().enabled = false;
		//GetComponent<Renderer>().material = material;
	}
}