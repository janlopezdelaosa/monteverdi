﻿using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Saves the general app information
/// SINGLETONE class: only one instance of the class is allowed
/// </summary>
public class AppManager : MonoBehaviour {

                                                             // Singletone:
    private static AppManager Instance;                      // · Ref to the only possible instantiated object

                                                             // Scenes management:
    public static string previousScene;                      // · Scenes are passed to class funcs as SceneAssets...
    public static string currentScene;                       //   ...but represented internally as strings

                                                             // Fading:
    public static Fading fader;                              // · Object in charge of fading transition effect

                                                             // Language support:
    public static Translator translator;                     // · Object in charge of translating texts
    

    public static string language                            // · Current language of the app...
    {
        get { return Translator.language;  }                 //   ...storaged in the "Translator" GameObject
        set { Translator.language = value; }
    }

    public void SetLanguage(string value)                    // · Call it for a language change
    {
        if (value != Translator.language)                    //   If changing to a different language...
        {
            Translator.language = value;
            reloadScene();                                   //   ...the scene needs to be reloaded for the texts to upload
        }
    }

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);                   // The object will survive the entire game
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        currentScene = SceneManager.GetActiveScene().name;

        fader = GameObject.Find("Fading").GetComponent<Fading>();

        translator = GameObject.Find("Translator").GetComponent<Translator>();
    }

    void Update()                                            // For debugging purposes
    {
        Debug.Log("Current:  " + currentScene + "\nPrevious: " + previousScene);
        Debug.Log("language: " + language);
        Debug.Log("Dictionary: " + Translator.appTexts.Count);
        foreach ( string s in Translator.appTexts.Keys)
        {
            Debug.Log("Keys: " + s);
        }

    }

    /// <summary>
    /// Loads the specified scene and updates
    /// current/previous scene information
    /// </summary>
    /// <param name="nextScene"></param>
    public void loadScene(SceneAsset nextScene)
    {
        previousScene = currentScene;
        currentScene = nextScene.name;

        fader.FadeToScene(currentScene);
    }

    /// <summary>
    /// Loads the previous scene and updates
    /// current/previous scene information
    /// </summary>
    public void loadPreviousScene()
    {
        string aux = previousScene;
        previousScene = currentScene;
        currentScene = aux;

        fader.FadeToScene(currentScene);
    }

    /// <summary>
    /// Reloads the current scene and doesn't 
    /// update current/previous scene info 
    /// (otherwise both would refer to the current)
    /// </summary>
    public void reloadScene()
    {
        fader.FadeToScene(currentScene);
    }
}
