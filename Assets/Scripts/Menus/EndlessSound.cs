﻿using UnityEngine;

/// <summary>
/// Makes the sound play in an infinite loop
/// SINGLETONE class: only one instance of the class is allowed
/// </summary>
[RequireComponent (typeof(AudioSource))]                        // An AudioSource component is required for endless sound
public class EndlessSound : MonoBehaviour {

    private static EndlessSound Instance;                       // Ref to the only possible instantiated object
    private static bool mute = false;                           // "All the objects" must behave equally
    private AudioSource audioComponent;                         // Ref to each object's AudioSource

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);                      // The object will survive the entire game
            Instance = this;
            audioComponent = GetComponent<AudioSource>();
            audioComponent.playOnAwake = true;                  // Make sure the audio will start playing at the beginning...
            audioComponent.enabled = true;                      // ...for ever
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        audioComponent.mute = mute;
    }

    public void ToggleMute(bool value)
    {
        mute = value;
    }
}