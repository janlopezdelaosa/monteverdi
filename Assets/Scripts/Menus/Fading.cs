﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Fading : MonoBehaviour {

    private static Fading Instance;                       // Ref to the only possible instantiated object

    [Tooltip("Black texture used to achieve the effect\nCheck \"Textures/Menus/fadingBlack\"")]
    public Texture2D fadeOutTexture;
    [Tooltip("Determines how long the fading lasts")]
    public float fadingSpeed = 0.8f;
    [Tooltip("Determines how much time there is between the click and the beginning of fading")]
    public float fadeResponseDelay = 0.0f;

    private int drawDepth = -1000;                        // ???
    private float alpha = 0.0f;                           // (Transparency) 0 -> 0% opaque, 1.0 -> 100% opaque
    private int fadeDir = -1;                             // 1: White -> Black, -1: Black -> White


    void Start() {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);                // The object will survive the entire game
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void OnGUI() {
        alpha += fadeDir * fadingSpeed * Time.deltaTime;
        alpha = Mathf.Clamp01(alpha);

        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
    }

    public void BeginFade() { // (int direction) {
        fadeDir = 1; //direction;
        //return (fadeSpeed);
    }

    void OnLevelWasLoaded() {
        fadeDir = -1;
    }

    public void FadeToScene(string currentScene)          // Previously named "Pulsacion"
    {
        StartCoroutine(FadeToSceneInTime(currentScene, fadeResponseDelay));
    }

    IEnumerator FadeToSceneInTime(string scene, float t)
    {
        yield return new WaitForSeconds(t);
        BeginFade();
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(scene);
    }
}