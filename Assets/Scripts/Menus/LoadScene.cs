﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

[RequireComponent(typeof(Button))]
public class LoadScene : MonoBehaviour {

    [Tooltip("The scene the button will load.nMake sure the scene is included in the Build settings.\nButton component required!")]
    public SceneAsset scene;
    
    public void Load()
    {
        SceneManager.LoadScene(scene.name);
    }
}
