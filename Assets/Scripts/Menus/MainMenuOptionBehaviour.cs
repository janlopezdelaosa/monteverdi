﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This option button moves from "Option" (parent) button
/// to its final position (child "UnfoldedPosition" object)
/// The button becomes visible at the moment its parent is
/// pressed (and starts moving)
/// The button becomes invisible at the moment it reaches
/// back its parent (and stops moving)
/// </summary>
public class MainMenuOptionBehaviour : MonoBehaviour {
    
    private RectTransform rect;                         // current option's position
    private Vector3 targetPos;                          // position to move, either of the two following position:
    private Vector3 parentPos;                          // · initial position of the object (same as its "Options" button parent)
    private Vector3 unfoldPos;                          // · position the option is moving to (indicated by child object)
    private float unfoldingSpeed;                       // How fast the buttons move to the next position...
    private const float SPEED_FACTOR = 0.02f;           // ...depends on the screen height and this speed multiplier

    void Start ()
    {
        rect = GetComponent<RectTransform>();
        parentPos = transform.parent.GetComponent<RectTransform>().position;
        unfoldPos = transform.GetChild(0).GetComponent<RectTransform>().position;
        targetPos = unfoldPos;                          // Assign unfoldPos first or swap in Move() won't work
        unfoldingSpeed = Screen.height * SPEED_FACTOR;  // Depends on the screen height and a multiplying factor

    }
	
	void Update ()
    {
        rect.position = Vector3.MoveTowards(rect.position, new Vector3(targetPos.x, targetPos.y, targetPos.z), unfoldingSpeed);
        if (targetPos == parentPos && rect.position.y <= parentPos.y)       // If option is withdrawing...
            transform.gameObject.SetActive(false);                          // ...make invisible when it reaches the parent
	}

    public void Move()
    {
        targetPos = (targetPos != unfoldPos) ? unfoldPos : parentPos;       // Target position alternates between positions
        if (targetPos == unfoldPos)                                         // If option is unfolding...
            transform.gameObject.SetActive(true);                           // ...make visible inmediately
    }
}
