﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Bolinche
{
    [Tooltip("Image to be displayed as Bolinche\nIf any, no bolinches bar will be shown")]
    public Sprite bolinchesImage;        // The image to be used as a "bolinche" (a point)
    [Tooltip("Width (& Height), it's a square image")]
    public float width;                  // Width (and Height) of the image
    [Tooltip("Distance between bolinches")]
    public float padding;                // The distance between the images
}


[RequireComponent(typeof(ScrollRect))]
public class SetScroll : MonoBehaviour {


    public Sprite[] scrollImages;               // The array of images this scene is going to show
    private RectTransform content;              // The content object where the images will be placed
    private GameObject[] imagesList;            // List of the actual image objects once they're instantiated

    public Bolinche bolincheInfo;               // Image, width & padding (see Bolinche class above)
    private RectTransform bolinches;            // The bolinche object where the bolinches will be placed
    private GameObject[] bolinchesList;         // List of the actual bolinche objects, once they're instantiated

    private Dictionary<int, int> multipliers;   // Contains multiplying factors for the position of the bolinches

    Dictionary<int, int> generateDictionary(int numImages, bool debug  = false)
    {
        /* 
        Example of numImages input and Dictionary output:
        numImages: 1, Dictionary<int, int> { { 1, 0 } }

        numImages: 2, Dictionary<int, int>{ { 1, 0 },
                                            { 2, 0 } }

        numImages: 3, Dictionary<int, int>{ { 1, -1 },
                                            { 2,  0 },
                                            { 3,  1 } }

        numImages: 4, Dictionary<int, int>{ { 1, -1 },
                                            { 2,  0 },
                                            { 3,  0 },
                                            { 4,  1 } }

        numImages: 5, Dictionary<int, int>{ { 1, -2 },
                                            { 2, -1 },
                                            { 3,  0 },
                                            { 4,  1 },
                                            { 5,  2 } }

        numImages: 6, Dictionary<int, int>{ { 1, -2 },
                                            { 2, -1 },
                                            { 3,  0 },
                                            { 4,  0 },
                                            { 5,  1 },
                                            { 6,  2 } }
        */

        Dictionary<int, int> dictionary = new Dictionary<int, int>();

        int negative_i;                              // index descending from base/left_base to 1
        int positive_i;                              // index increasing from base/right_base to numImages

        if (!isEven(numImages))                      // Odd distributions centered in one number (base_i)
        {
            int base_i = (numImages + 1) / 2;
            dictionary[base_i] = 0;
            negative_i = base_i - 1;
            positive_i = base_i + 1;
        }
        else                                         // Even distributions centered in two numbers (left_base_i and right_base_i)
        {
            int left_base_i = numImages / 2;
            int right_base_i = (numImages + 2) / 2;
            dictionary[left_base_i] = 0;
            dictionary[right_base_i] = 0;
            negative_i = left_base_i - 1;
            positive_i = right_base_i + 1;
        }

        for (int multiplier = 1; negative_i > 0; negative_i--, positive_i++, multiplier++)
        {
            dictionary[negative_i] = multiplier * -1;
            dictionary[positive_i] = multiplier;
        }

        /*
        int multiplier = 1;                          // The value of the dictionary
        while (negative_i > 0)                       // or (positive_i > numImages)
        {
            dictionary[negative_i] = multiplier * -1;
            dictionary[positive_i] = multiplier;
            negative_i--; positive_i++; multiplier++;
        }
        */

        if (debug)
        {
            foreach (int i in multipliers.Keys)
            {
                Debug.Log("key: " + i + ". Value: " + multipliers[i]);
            }
            Debug.Log(multipliers);
        }

        return dictionary;
    }

    void Awake () {

        multipliers = generateDictionary(scrollImages.Length);

        content = transform.GetChild(0).GetComponent<RectTransform>();  // "Content" is the 1st child of the scroll
        content.localScale =new Vector3(scrollImages.Length, 1, 1);     // Multiply X's Content by the number of images so all of them can fit in there
        bolinches = transform.GetChild(1).GetComponent<RectTransform>();// "Bolinches" is the 2nd child of the scroll

        float xStep = 1f / (float)scrollImages.Length;                  // Divides Content's width in equal -and normalized- segments
        imagesList = new GameObject[scrollImages.Length];               // Initializes the list of image-objects to its prope size
        int numBolinches = scrollImages.Length;
        if (bolincheInfo.bolinchesImage != null)
            bolinchesList = new GameObject[numBolinches];               // Initializes the list of bolinche-objects to its proper size
        else
            bolinchesList = new GameObject[0];

        for (int i = 0; i < numBolinches; i++)
        {
            #region Set Image

            GameObject go = new GameObject("Image" + i);
            imagesList[i] = go;                                         // Saves an images object list
            Image image = go.AddComponent<Image>();
            image.sprite = scrollImages[i];
            image.transform.parent = content;                           // First, set image as a child of content
            
            // The rectangle enclosing the image:
            //          _____MAX.Y_____
            //          |              |
            //          M              M
            //          I              A
            //          N              X
            //          x              X
            //          |____MIN.Y_____|
            // · AnchorMin.X value is the  left    vertical   line. f(x, y) = f'(x = AnchorMin.X, y)
            // · AnchorMin.Y value is the   top   horizontal  line. f(x, y) = f'(x, y = AnchorMax.Y)
            // · AnchorMax.X value is the  right   vertical   line. f(x, y) = f'(x = AnchorMax.X, y)
            // · AnchorMax.Y value is the  bottom horizontal  line. f(x, y) = f'(x, y = AnchorMax.Y)
            float xMin = xStep * i;                                     // Fraction of the Content's width where the image should begin...
            float xMax = xStep * (i + 1);                               // ...and end
            image.rectTransform.anchorMin = new Vector2(xMin, 0);       
            image.rectTransform.anchorMax = new Vector2(xMax, 1);

            image.rectTransform.pivot = new Vector2(0, 0.5f);           // Origin of the axes: left of the image, middle height

            // If anchors are different "position values" come to be "padding values":
            // if (AnchorMax.Y != AnchorMin.Y)
            //     offsetMin.Y: PosY   -> Top
            //     offsetMax.Y: Height -> Bottom
            // if (AnchorMax.X != AnchorMin.X)
            //     offsetMin.X: PosX  -> Left
            //     offsetMax.X: Width -> Right
            image.rectTransform.offsetMin = new Vector2(0, 0);          // (Left, Top) padding. (No padding)
            image.rectTransform.offsetMax = new Vector2(0, 0);          // (Right, Bottom) padding. (No padding)

            image.transform.localScale = new Vector3(1,  1, 1);         // Set the image to ocuppy its whole given space

            #endregion

            if (bolincheInfo.bolinchesImage != null)
            {
                #region Set Bolinches

                go = new GameObject("Bolinche" + i);                    // Reuse -auxiliary- image's go
                bolinchesList[i] = go;                                  // Save a bolinches object list
                Image bol = go.AddComponent<Image>();
                bol.sprite = bolincheInfo.bolinchesImage;
                bol.transform.parent = bolinches;                       // First, set bol(inche) as a child of bolinches


                bol.rectTransform.anchorMin = new Vector2(0.5f, 0.5f);  // Align anchors and pivot with the center of the image
                bol.rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
                bol.rectTransform.pivot = new Vector2(0.5f, 0.5f);

                float xPos;                                             // Position of the bolinche along the horizontal axe
                xPos = calculateBolinchePosition(numBolinches, i + 1,   // Depends on the number of bolinches, the number of this bolinche,
                                                 bolincheInfo.width,    // ...how wide bolinches are... 
                                                 bolincheInfo.padding); // ...and the space between them

                bol.rectTransform.localPosition = new Vector2(xPos, 0); // (PosX, PosY)
                bol.rectTransform.sizeDelta = new Vector2(bolincheInfo.width, bolincheInfo.width); // (Width, Height) Square image
                
                #endregion
            }

        }
    }

    float calculateBolinchePosition(int nB, int i, float width, float padding)
    {
        // Between each bolinche the dist is:
        // · Distance from the center to the limit of the image                       (width / 2)
        // · Distance between images                                                    padding
        // · Distance between the limit of the image to the center of the next image  (width / 2)
        //                                                                       ---------------------
        float baseDistance = (width + padding);                          //         (width + padding)
        float finalDistance = baseDistance * multipliers[i];             // dict containing factors (hardcoded)

        if (isEven(nB))                                                      // No bolinche at x = 0, between center bolinches 
        {                                                                    // the usual distance needs to be compensated...:
            if (i <= nB / 2) return finalDistance - ((width + padding) / 2); // ...to the left side
            if (i >  nB / 2) return finalDistance + ((width + padding) / 2); // ...and to the right side
        }
        return finalDistance;
    }

    public static bool isEven(int value) { return value % 2 == 0; }

    public GameObject[] getBolinches() { return bolinchesList; }

    public GameObject[] getScrollImages() { return imagesList; }
    
}
