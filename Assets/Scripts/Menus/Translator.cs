﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Xml;
using System.Collections.Generic;

/// <summary>
/// Auxiliary class defining languages
/// (according to ISO 639...?)
/// </summary>
public class Lang
{
    public const string en_US = "en_US";
    public const string es_Es = "es_ES";
}

public class Translator : MonoBehaviour {

    private static Translator Instance;                                      // Ref to the only possible instantiated object

    public static string language;                                           // Current app language

    public static Dictionary<string, Dictionary<string, string>> appTexts;   // General app texts, appTexts[escena][text]: traduccion
    private Dictionary<string, string> currentSceneTexts;                    // Current scene's text, textos[text]: traducción

    void Start()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);                                   // The object will survive the entire game
            Instance = this;

            language = Lang.en_US;                                           // 1st time, set language to english by default
            appTexts = new Dictionary<string, Dictionary<string, string>>(); // Initialize dictionary
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        LoadLanguage();                                                      // Any time the scene is started, load texts according to current lan

    }

    /// <summary>
    /// Load texts (if necessary) and update them
    /// Loading made from English texts (so english text should be
    /// found in the text property of the Text components. This is 
    /// only guaranteed if the scene has not changed language yet,
    /// this is why changing language needs reloading the scene.
    /// </summary>
    /// <param name="language"></param>
    public void LoadLanguage()
    {
        // If the language is "en_US" the text is already in the app:
        //     · dont' do anything
        // If another language is loaded for the 1st time (appTexts[scenename] is empty):
        //     · read xml
        //     · update scene's texts
        // If another language is loaded for the nth time:
        //     · get info from App.traducciones[xcenename]
        //     · update scene's texts

        if (language != Lang.en_US)
        {
            if (!appTexts.TryGetValue(AppManager.currentScene, out currentSceneTexts))              // If this scene's texts aren't loaded...
            {
                currentSceneTexts = new Dictionary<string, string>();                               // Create auxiliary dictionary
                TextAsset xmlplain = (TextAsset)Resources.Load("Languages/" + AppManager.language); // Load (text) resource
                XmlDocument xmldoc = new XmlDocument();                                             // Create xml object
                xmldoc.LoadXml(xmlplain.text);                                                      // Load text as xml
                XmlNodeList hijos = xmldoc.SelectSingleNode("strings/" + AppManager.currentScene).ChildNodes; // "hijos" are all the specified strings (for this scene) in the xml

                foreach (XmlNode node in hijos)
                    currentSceneTexts.Add(node.Attributes.GetNamedItem("id").Value, node.Attributes.GetNamedItem("trans").Value);

                appTexts.Add(AppManager.currentScene, currentSceneTexts);
            }
            UpdateTextsLanguage();
        }
    }

    /// <summary>
    /// Change all texts in the scene to the current language
    /// </summary>
    private void UpdateTextsLanguage()
    {
        string trans;
        foreach (GameObject gbtt in SceneManager.GetActiveScene().GetRootGameObjects())          // Get all root objects
        {
            foreach (Text t in gbtt.GetComponentsInChildren<Text>(true))                         // All text components, even in the inactive objects
            {
                if (currentSceneTexts.TryGetValue(t.text, out trans))
                    t.text = trans;                                                              // Set text value to its translation
                else
                    Debug.LogWarning("A text with no translation was found: " + t.text);
            }
        }
    }

    public static string translate(string id) {
        Dictionary<string, string> textos;
        if (appTexts.TryGetValue(SceneManager.GetActiveScene().name, out textos)) {
            string trans;
            if (textos.TryGetValue(id, out trans))
                return trans;
            else
                return id;
        }
        else
            return id;
    }
}
