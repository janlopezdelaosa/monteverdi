﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.SceneManagement;

public class __TimeToNextLevel : MonoBehaviour {

    public float time;
    public SceneAsset nextScene;

	void Start ()
    {
        StartCoroutine(wait(time));
	}

    IEnumerator wait(float t)
    {
       yield return new  WaitForSeconds(t);
       GameObject.Find("AppManager").GetComponent<AppManager>().loadScene(nextScene);
       //SceneManager.LoadScene("MainMenu");
    }
}
