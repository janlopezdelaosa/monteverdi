﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The tools bar of the visorn moves from "CanvasTools" (parent)
/// position to its final position (child "UnfoldedPosition" object)
/// The tooggle changes its image
/// </summary>
public class ToolsBarBehaviour : MonoBehaviour
{
    [Tooltip("\"+\" image indicating the button to fold the bar")]
    public Sprite fold;
    [Tooltip("\"-\" image indicating the button to unffold the bar")]
    public Sprite unfold;

    private RectTransform rect;                         // current bar's position (CanvasTools" father's position)
    private Vector3 targetPos;                          // position to move, either of the two following position:
    private Vector3 parentPos;                          // · initial position of the object (same as its "CanvasTools" parent)
    private Vector3 unfoldPos;                          // · position the bar is moving to (indicated by child object)
    private float unfoldingSpeed;                       // How fast the bar moves to the next position...
    private const float SPEED_FACTOR = 0.02f;           // ...depends on the screen width and this speed multiplier
    private Image image;                                // This image's sprite: Fold)+) / Unfold(-)

    void Start()
    {
        rect = transform.parent.GetComponent<RectTransform>();
        parentPos = transform.parent.GetComponent<RectTransform>().position;
        unfoldPos = transform.GetChild(0).GetComponent<RectTransform>().position;
        targetPos = parentPos;                          // Assign unfoldPos first or swap in Move() won't work
        unfoldingSpeed = Screen.width * SPEED_FACTOR;   // Depends on the screen width and a multiplying factor
        image = GetComponent<Image>();                  // The moving GO is the parent but the image change is in this GO
    }

    void Update()
    {
        rect.position = Vector3.MoveTowards(rect.position, new Vector3(targetPos.x, targetPos.y, targetPos.z), unfoldingSpeed);
        if (targetPos == parentPos)
            image.sprite = fold;                        //change sprite to '+'
        if (targetPos == unfoldPos)
            image.sprite = unfold;                      //change sprite to '-'
    }

    public void Move()
    {
        targetPos = (targetPos != unfoldPos) ? unfoldPos : parentPos;          // Target position alternates between positions
    }
}
