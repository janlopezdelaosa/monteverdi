﻿using UnityEngine;
using UnityEngine.UI;
//using Vuforia;

[RequireComponent(typeof(Image))]
public class VisorButtonFlash : MonoBehaviour
{
    [Tooltip("Sprite to show when button can be click to set the flash ON")]
    public Sprite flashOn;
    [Tooltip("Sprite to show when button can be click to set the flash OFF")]
    public Sprite flashOff;

    private Image image;                    // Reference to the image component

    void Start()
    {
        image = GetComponent<Image>();
    }

    public void FlashOnOff(bool triggerValue)
    {
        if (triggerValue)
        {
            image.sprite = flashOn;
            //CameraDevice.Instance.SetFlashTorchMode(true);    // TODO: Add Vuforiato the project
        }
        else
        {
            image.sprite = flashOff;
            //CameraDevice.Instance.SetFlashTorchMode(false);   // TODO: Add VUFORIA to the project
        }
    }
}
