﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// De momento el botón Reload tan sólo vuelve a cargar la pantalla otra vez
/// </summary>
public class VisorButtonReload : MonoBehaviour {

	public void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}

/*
 * Clase Recarga del proyecto Chromville
 * que básicamente resetea valores de la
 * clase General. Aquí no existe todavía
 * (aunque AppManager pretende hacer eso
 * mismo). Guardo aquí para el futuro:
using UnityEngine;
using System.Collections;

public class Recarga : MonoBehaviour {

	//Clean the scene and recharge the scene again
	public void OnClick () {
		General.desaparecer=true;	
		General.tiempo=0;
		General.activado = false;
		General.pulsadoLimpiar=true;
		//SpritesRandom.cargar = true;
		General.limpio = true;
		General.NombreLaminaCaptura = "";
		this.gameObject.GetComponent<AudioSource>().Play();
	}
}
*/
