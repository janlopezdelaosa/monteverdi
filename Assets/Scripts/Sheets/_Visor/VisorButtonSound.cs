﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class VisorButtonSound : MonoBehaviour
{
    [Tooltip("Sprite to show when button can be click to set the sound ON")]
    public Sprite soundON;
    [Tooltip("Sprite to show when button can be click to set the sound OFF")]
    public Sprite soundOFF;

    private Image image;                        // Reference to this GO's image component

    void Start()
    {
        image = GetComponent<Image>();
        if (GameObject.Find("BackgroundMusic").GetComponent<AudioSource>().mute)
        {
            image.sprite = soundON;             // Music is mute, the button reflects what happend will if you click it (sound will turn on)
            GetComponent<Toggle>().isOn = true; // "isOn" can be read as "isMute"
        }
        else
        {
            image.sprite = soundOFF;
            GetComponent<Toggle>().isOn = false;
        }
    }

    public void SoundOnOff(bool triggerValue)
    {
        //if (triggerValue)                     // If "triggerValue" == "isOn" == "isMute"
        //    image.sprite = soundON;
        //else
        //    image.sprite = soundOFF;
        image.sprite = (triggerValue) ? soundON : soundOFF;
    }
}
