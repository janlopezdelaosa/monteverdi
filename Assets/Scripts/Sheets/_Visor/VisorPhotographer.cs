using UnityEngine;
using System.Collections;

public class VisorPhotographer: MonoBehaviour {

    [Tooltip("Herramientas padre")]
	private GameObject HijoRoot;                 // Herramientas padre
    [Tooltip("FotoRealizada (Imagen \"Saved\" y textos)")]
    private GameObject FotoRealizada;            // FotoRealizada (Imagen "Saved" y textos)
    [Tooltip("Back, Esquinas y el resto de elementos de la imagen")]
    private GameObject [] Interfaz;              // Back, Esquinas y el resto de elementos de la imagen

	private float tiempo;
	private float contador;

    [Tooltip("\"Public\" for debugging purposes")]
    bool hideGUI = false;

    //bool savedScreenshot = false;               // S�lo se pone a true una vez en ScreenshotSaved, why? Para qu� vale?
    //bool savedExistingImage = false;            // S�lo se pone a true una vez en ImageAssetSaved, why? Para qu� vale?
    //bool captura = false;                       // "S�lo" se pone a false (no cambia) una vez, en hideGUI, why? Para qu� vale?
    private bool activadorfoto;                   // 

    public bool foto = false;

    void Start()
    {
        // call backs
        tiempo = Time.time;
        contador = Time.time;
        //ScreenshotManager.ScreenshotFinishedSaving += ScreenshotSaved;
        //ScreenshotManager.ImageFinishedSaving += ImageAssetSaved;

        HijoRoot = transform.parent.gameObject;
        Interfaz = new GameObject[HijoRoot.transform.childCount - 1];
        for (int i = 0; i < HijoRoot.transform.childCount; i++)
        {
            GameObject auxGO = HijoRoot.transform.GetChild(i).gameObject;
            if (auxGO.name != transform.name)
                Interfaz[i] = auxGO;
        }
        FotoRealizada = transform.GetChild(0).gameObject;

    }

    void Update(){
		if(activadorfoto == true){
			if(Time.time-contador>2f)
            {
				FotoRealizada.SetActive(false);
				//HijoRoot.SetActive(true);
				for(int i= 0; i<=Interfaz.Length-1; i++)
                {
                    //if (General.NombreLaminaCaptura == Interfaz[i].name || Interfaz[i].name == "Back" || Interfaz[i].name == "Esquinas")

						Interfaz[i].SetActive(true);
				}
				contador = 0;
				activadorfoto = false;	
			}	
		}
		
		if(!hideGUI)
		{
			// option 1 - save a screenshot
			if(foto == true)
            {	
				//HijoRoot.SetActive(false);
				for(int i= 0; i<=Interfaz.Length-1; i++)
                {
					Interfaz[i].SetActive(false);
				}
                StartCoroutine(HideGUI());
			}
		}
	}

    public void SetFoto(bool value)
    {
        foto = value;
    }

    IEnumerator Boo()
    {
        yield return new WaitForSeconds(0f);
        Debug.Log("Works");
        ;
    }

    IEnumerator HideGUI ()
	{
		foto = false;
		yield return new WaitForSeconds(0.5f);
		StartCoroutine(ScreenshotManager.Save("Chromville_Science", "Chromville_Science", true));	
		yield return new WaitForSeconds(0.8f);
		FotoRealizada.SetActive(true);
		hideGUI = true;
		//captura = false;	
		yield return new WaitForSeconds(0.8f);
		hideGUI = false;	
		contador = Time.time;
		activadorfoto = true;
	}


    
 //   void ScreenshotSaved(string path)
	//{
	//	//savedScreenshot = true;	
	//}
	

	//void ImageAssetSaved(string path)
	//{
	//	//savedExistingImage = true;
	//	HijoRoot.SetActive(true);
	//	for(int i= 0; i<=Interfaz.Length-1; i++){
	//		Interfaz[i].SetActive(true);
	//	}
	//}
}